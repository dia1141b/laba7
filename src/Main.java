/**
 * 1.Переопределить метод getName в классе Whale(Кит),
 * чтобы программа выдавала: Я не корова, Я – кит.
   2.Написать метод, который определяет, объект какого класса ему передали,
   * и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
   3.Создать два класса Cat(кот) и Dog(собака), класс Dog(собака)
   * происходит от Cat(кот). Определить метод getChild в классах Cat(кот)
   * и Dog(собака), чтобы кот порождал кота, а собака – собаку.
 
 */

/**
 *
 * @author Администратор
 */
public class Main {
    
    public static void test(Object obj) {
        if (obj instanceof Cat) {
            System.out.println("Объект класса Cat!");
        } else if (obj instanceof Dog) {
            System.out.println("Объект класса Dog!");
        } else if (obj instanceof Bird) {
            System.out.println("Объект класса Bird!");
        } else if (obj instanceof Lamp) {
            System.out.println("Объект класса Lamp!");
        }
    }
     public static int print(int a) {
        return a;
    }

    public static Integer print(Integer b) {
        int i = b.intValue() * 5;
        return i;
    }
    public static void main(String[] args) {
        Whale Whale= new Whale();
        Whale.printAll();//1z P
        
        
        Cat cat = new Cat();
        Dog dog = new Dog();
        Bird bird = new Bird();
        Lamp lamp = new Lamp();
        test(cat);//2z P
        
        
          Kot kot = new Kot();
        Pes pes = new Pes();
        Kot kot1 = kot.getChild();
        System.out.println(kot);
        System.out.println(kot1);//3z P
        
        
        
        int a = 10;
        Integer b = new Integer(100);

        System.out.println(print(a));
        System.out.println(print(b));//2z P_M
        
      

    }
 
}
