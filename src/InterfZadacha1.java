
import java.util.ArrayList;

/*
 Реализовать два интерфейса PassangersAuto(описать метод перевозки пассажиров)
и CargoAuto (описать метод перевозки груза). Написать классы Truck, Sedan, 
Pickup реализующие эти интерфейсы
 *
 * @author Администратор
 */
public class InterfZadacha1 {

    interface PassangersAuto {

        void transportPass();
    }

    interface CargoAuto {

        void transportCargo();
    }

    static class Truck implements CargoAuto {

        @Override
        public void transportCargo() {
            System.out.println("Перевозит груз");
        }
    }

    static class Sedan implements PassangersAuto {

        @Override
        public void transportPass() {
            System.out.println("Перевозит пассажиров");
        }
    }

    static class Pickup implements PassangersAuto, CargoAuto {

        @Override
        public void transportPass() {
            System.out.println("Перевозит пассажиров");
        }

        @Override
        public void transportCargo() {
            System.out.println("Перевозит груз");
        }
    }

    public static void main(String[] args) {
        ArrayList auto = new ArrayList();
        auto.add(new Truck());
        auto.add(new Sedan());
        auto.add(new Pickup());

        for (Object transport : auto) {
            if (transport instanceof PassangersAuto) {
                PassangersAuto transPass = (PassangersAuto) transport;
                transPass.transportPass();
            }
        }
    }
}
