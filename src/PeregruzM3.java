/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Администратор
 */
public class PeregruzM3 {
    public static int print(int a) {
        return a;
    }

    public static long print(long b) {
        return b;
    }

    public static double print(double c) {
        return c;
    }

    public static float print(float d) {
        return d;
    }

    public static byte print(byte e) {
        return e;
    }

    public static void main(String[] args) {
        int a = (int) 1234567890;
        long b = 9223372036854775807l;
        double c = (double) 123.45;
        float d = (float) 12.345678;
        byte e = (byte) 123;

        System.out.println("\n" + print(a));
        System.out.println(print(b));
        System.out.println(print(c));
        System.out.println(print(d));
        System.out.println(print(e));
    }
}
