
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Администратор
 */
public class PeregruzM4 {

    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    public static long min(long x, long y) {
        if (x < y) {
            return x;
        } else {
            return y;
        }
    }

    public static double min(double m, double n) {
        if (m < n) {
            return m;
        } else {
            return n;
        }
    }

    public static void main(String[] args) {
        int a = 0, b = 0;
        long x = 0, y = 0;
        double n = 0, m = 0;
        try {
            System.out.println("Введите числа типа int:");
            Scanner sc_a = new Scanner(System.in);
            a = sc_a.nextInt();
            Scanner sc_b = new Scanner(System.in);
            b = sc_b.nextInt();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        try {
            System.out.println("Введите числа типа long:");
            Scanner sc_x = new Scanner(System.in);
            x = sc_x.nextLong();
            Scanner sc_y = new Scanner(System.in);
            y = sc_y.nextLong();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        try {
            System.out.println("Введите числа типа double:");
            Scanner sc_m = new Scanner(System.in);
            m = sc_m.nextDouble();
            Scanner sc_n = new Scanner(System.in);
            n = sc_n.nextDouble();
        } catch (Exception ex) {
            System.out.println("Ошибка ввода " + ex);
        }
        System.out.println("Наименьшее число типа int:" + min(a, b));

        System.out.println("Наименьшее число типа long:" + min(x, y));

        System.out.println("Наименьшее число типа double:" + min(m, n));

    }
}
